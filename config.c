#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <alsa/asoundlib.h>
#include <linux/uinput.h>

#include "config.h"

DEFINE_LIST_FUNCTIONS(struct mt_filter, mt_filter_list)

static unsigned char channel = 0;

int process_line(char *line, int len, struct mt_config *config) {
	if(line[0] == '#') {
		// Comment
		return 0;
	}

	if(line[0] == '@') {
		// Directive
		if(!strcmp(line, "@dump")) {
			config->dump = true;
		} else if(!strcmp(line, "@nodump")) {
			config->dump = false;
		} else if(!strcmp(line, "@notes")) {
			config->print_notes = true;
		} else if(!strcmp(line, "@nonotes")) {
			config->print_notes = false;
		} else if(!strncmp(line, "@channel", 8)) {
			sscanf(line, "@channel %hhu", &channel);
		} else if(!strncmp(line, "@pedal", 6)) {
			struct mt_filter filter = {
				.type = SND_SEQ_EVENT_CONTROLLER,
				.channel = channel,
				.param = 64,
			};
			sscanf(line, "@pedal %u", &filter.key);
			if(append_mt_filter_list(&config->filters, filter)) {
				perror(NULL);
				return -1;
			}
		} else {
			fprintf(stderr, "WARNING: unrecognised directive: %s\n", line);
		}
		return 0;
	}

	if(line[0] == '$') {
		// Filter
		struct mt_filter filter = {
			.type = SND_SEQ_EVENT_NOTEON,
			.channel = channel,
		};
		int x;
		x = sscanf(line, "$%hhu,%u", &filter.note, &filter.key);
		if(x != 2) {
			if(x < 0) {
				perror(NULL);
			} else {
				fprintf(stderr, "ERROR: invalid filter\n");
			}
			return -1;
		}
		if(filter.type != SND_SEQ_EVENT_NOTEON) {
			fprintf(stderr, "WARNING: unsupported event type: %d\n", filter.type);
			return -1;
		}
		if(append_mt_filter_list(&config->filters, filter)) {
			perror(NULL);
			return -1;
		}
		return 0;
	}
	return 0;
}

int load_config(struct mt_config *config, const char *file) {
	*config = (struct mt_config){0};

	FILE *f = fopen(file, "r");
	if(!f) {
		perror(file);
		return -1;
	}

	char *line = NULL;
	size_t size = 0;
	ssize_t len;
	int parse_failure = 0;

	while((len = getline(&line, &size, f)) >= 0) {
		if(line[len -1] == '\n') {
			line[--len] = 0;
		}
		if(len > 0) {
			if(process_line(line, len, config) != 0) {
				parse_failure = 1;
				break;
			}
		}
	}

	free(line);

	int fail = ferror(f);
	fail |= (fclose(f) != 0);
	if(fail) {
		perror(file);
	}
	return fail | parse_failure;
}

void free_config(struct mt_config *config) {
	free(config->filters.data);
}
