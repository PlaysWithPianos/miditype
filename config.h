#ifndef MIDITYPE_CONFIG_H
#define MIDITYPE_CONFIG_H 1

#include <stdbool.h>
#include <alsa/asoundlib.h>

#include "arraylist.h"

struct mt_filter {
	snd_seq_event_type_t type;
	unsigned char channel;
	unsigned char note;
	unsigned param;
	unsigned key;
};

DECLARE_LIST(struct mt_filter, mt_filter_list)

struct mt_config {
	bool dump;
	bool print_notes;
	mt_filter_list filters;
};

int load_config(struct mt_config *config, const char *file);
void free_config(struct mt_config *config);

#endif /* MIDITYPE_CONFIG_H */
