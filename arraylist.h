#ifndef ARRAY_LIST_H
#define ARRAY_LIST_H 1

#include <stddef.h>
#include <errno.h>
#include <stdlib.h>

#define DECLARE_LIST(type, name) \
typedef struct name { \
	int max; \
	int len; \
	type *data; \
} name; \
\
int append_##name(name *list, type x);

#define DEFINE_LIST_FUNCTIONS(type, name) \
int append_##name(name *list, type x) { \
	if(list->len >= list->max) { \
		int newmax = list->max ? list->max * 2 : 2; \
		type *newdata = realloc(list->data, newmax * sizeof(type)); \
		if(!newdata) { return errno; } \
		list->data = newdata; \
		list->max = newmax; \
	} \
	list->data[list->len++] = x; \
	return 0; \
}

#endif /* ARRAY_LIST_H */
