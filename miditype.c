#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>

#include <linux/uinput.h>
#include <alsa/asoundlib.h>

#include "config.h"

static snd_seq_t *client = NULL;
static int port_id;
static int queue_id;
static int fd = -1;
static struct mt_config config;

static volatile sig_atomic_t stop = 0;

void open_sequencer(const char *seqname, const char *fakename) {
	if(snd_seq_open(&client, seqname, SND_SEQ_OPEN_INPUT, 0) < 0){
		fprintf(stderr, "Failed to open sequencer.\n");
		exit(1);
	}
	snd_seq_set_client_name(client, fakename);
	port_id = snd_seq_create_simple_port(client, fakename, SND_SEQ_PORT_CAP_WRITE, SND_SEQ_PORT_TYPE_APPLICATION);
	queue_id = snd_seq_alloc_queue(client);
	snd_seq_start_queue(client, queue_id, NULL);
}

void close_sequencer(void) {
	if(client) {
		snd_seq_close(client);
	}
}

void create_fake_keyboard(const char *uinput_path, const char *fakename) {
	fd = open(uinput_path, O_WRONLY | O_NONBLOCK);
	if(fd < 0) {
		perror(uinput_path);
		exit(1);
	}

	// Enable keyboard input for standard keyboards
	ioctl(fd, UI_SET_EVBIT, EV_KEY);
	for(int i = 0; i < 128; i++) {
		ioctl(fd, UI_SET_KEYBIT, i);
	}

	struct uinput_user_dev dev;
	memset(&dev, 0, sizeof dev);
	dev.id.bustype = BUS_USB;
	snprintf(dev.name, UINPUT_MAX_NAME_SIZE, "%s", fakename);

	write(fd, &dev, sizeof dev);
	ioctl(fd, UI_DEV_CREATE);
}

void destroy_fake_keyboard(void) {
	if(fd >= 0) {
		ioctl(fd, UI_DEV_DESTROY);
		close(fd);
	}
}

static const char *note_names[12] = { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" };

void note_name(unsigned char x, char *name) {
	memset(name, 0, 4);
	int octave = x / 12 - 1;
	int note = x % 12;
	snprintf(name, 4, "%s%d", note_names[note], octave);
}

void send_event(int type, int code, int val) {
	struct input_event ie;

	ie.type = type;
	ie.code = code;
	ie.value = val;
	ie.time.tv_sec = 0;
	ie.time.tv_usec = 0;

	write(fd, &ie, sizeof ie);
}

void dump_event(const snd_seq_event_t *evt) {
	printf("{\n");
	printf("\ttype = %d\n", evt->type);
	printf("\tflags = %u\n", evt->flags);
	printf("\ttag = %u\n", evt->tag);
	printf("\tqueue = %u\n", evt->queue);
	printf("\ttime = %u.%u\n", evt->time.time.tv_sec, evt->time.time.tv_nsec);
	printf("\tsource = %u:%u\n", evt->source.client, evt->source.port);
	printf("\tdest = %u:%u\n", evt->dest.client, evt->dest.port);
	printf("\tdata = %.8x %.8x %.8x\n", evt->data.raw32.d[0], evt->data.raw32.d[1], evt->data.raw32.d[2]);
	switch(evt->type) {
		case SND_SEQ_EVENT_NOTEON:
			printf("\tnote = %d\n\tchannel = %d\n", evt->data.note.note, evt->data.note.channel);
			break;
		case SND_SEQ_EVENT_CONTROLLER:
			printf("\tparam = %d\n\tchannel = %d\n", evt->data.control.param, evt->data.note.channel);
			break;
	}
	printf("}\n");
}

bool filter_match(const snd_seq_event_t *evt, const struct mt_filter *filter) {
	if(filter->type != evt->type) { return false; }
	switch(filter->type) {
		case SND_SEQ_EVENT_NOTEON:
			return filter->note == evt->data.note.note && filter->channel == evt->data.note.channel;
		case SND_SEQ_EVENT_CONTROLLER:
			return filter->param == evt->data.control.param && filter->channel == evt->data.note.channel;
		default:
			return false;
	}
}

void process_event(const snd_seq_event_t *evt) {
	if(config.dump) {
		dump_event(evt);
	}

	for(int i = 0; i < config.filters.len; i++) {
		if(filter_match(evt, &config.filters.data[i])) {
			switch(evt->type) {
				case SND_SEQ_EVENT_NOTEON:
				{
					snd_seq_ev_note_t note = evt->data.note;
					// On my piano, velocity > 0 -> note pressed, = 0 -> note released
					if(note.velocity > 0) {
						if(config.print_notes) {
							char name[4];
							note_name(note.note, name);
							printf("%s: %d\n", name, note.velocity);
						}

						send_event(EV_KEY, config.filters.data[i].key, 1);
						send_event(EV_KEY, config.filters.data[i].key, 0);
						send_event(EV_SYN, SYN_REPORT, 0);
					}
					break;
				}
				case SND_SEQ_EVENT_CONTROLLER:
				{
					snd_seq_ev_ctrl_t ctrl = evt->data.control;
					send_event(EV_KEY, config.filters.data[i].key, ctrl.value > 0);
					send_event(EV_SYN, SYN_REPORT, 0);
					break;
				}
			}
		}
	}
}

void receive_events(unsigned char client_id, unsigned char port) {
	int x;

	snd_seq_addr_t src, dst;
	src.client = client_id;
	src.port = port;
	snd_seq_client_info_t *info;
	snd_seq_client_info_alloca(&info);
	snd_seq_get_client_info(client, info);
	dst.client = snd_seq_client_info_get_client(info);
	dst.port = port_id;

	snd_seq_port_subscribe_t *sub;
	snd_seq_port_subscribe_alloca(&sub);
	snd_seq_port_subscribe_set_sender(sub, &src);
	snd_seq_port_subscribe_set_dest(sub, &dst);
	snd_seq_port_subscribe_set_queue(sub, queue_id);
	snd_seq_port_subscribe_set_time_update(sub, 1);
	snd_seq_port_subscribe_set_time_real(sub, 1);
	x = snd_seq_subscribe_port(client, sub);

	if(x != 0) {
		perror(NULL);
		exit(EXIT_FAILURE);
	}

	fprintf(stderr, "Ready\n");

	snd_seq_event_t *evt;
	while(!stop) {
		if(snd_seq_event_input_pending(client, 1) > 0) {
			snd_seq_event_input(client, &evt);
			process_event(evt);
		} else {
			struct timespec ts = {
				.tv_sec = 0,
				.tv_nsec = 10000, // = 10 μs
			};
			nanosleep(&ts, NULL);
		}
	}
}

void sighandler(int sig) {
	stop = 1;
}

void usage(const char *argv0) {
	fprintf(stderr, "usage: %s [-c CONFIG] [-d UINPUT_DEVICE] [-n FAKE_NAME] [-s SEQUENCER] client_id[:port]\n", argv0);
	fprintf(stderr, "defaults:\n");
	fprintf(stderr, "  CONFIG = /etc/miditype\n");
	fprintf(stderr, "  UINPUT_DEVICE = /etc/uinput\n");
	fprintf(stderr, "  FAKE_NAME = miditype\n");
	fprintf(stderr, "  SEQUENCER = default\n");
	fprintf(stderr, "\nclient_id is the id of your instrument: use lsmidi to show connected clients\n");
	fprintf(stderr, "\nport is the port for instruments with multiple ports, default is 0\n");
	exit(EXIT_FAILURE);
}

void free_config_file(void) {
	free_config(&config);
}

int main(int argc, char **argv) {
	const char *config_path = "/etc/miditype";
	const char *uinput_path = "/dev/uinput";
	const char *fakename = "miditype";
	const char *seqname = "default";
	unsigned mididev = 0;
	unsigned port = 0;

	int opt;
	while((opt = getopt(argc, argv, "c:d:n:s:")) != -1) {
		switch(opt) {
			case 'c':
				config_path = optarg;
				break;
			case 'd':
				uinput_path = optarg;
				break;
			case 'n':
				fakename = optarg;
				break;
			case 's':
				seqname = optarg;
				break;
			default:
				usage(argv[0]);
		}
	}

	if(optind >= argc) {
		usage(argv[0]);
	}

	char *arg = strdup(argv[optind]);
	if(!arg) {
		perror(NULL);
		exit(EXIT_FAILURE);
	}
	char *colon = strchr(arg, ':');
	if(colon) {
		if(sscanf(arg, "%u:%u", &mididev, &port) != 2) {
			perror(NULL);
			exit(EXIT_FAILURE);
		}
	} else {
		if(sscanf(arg, "%u", &mididev) != 1) {
			perror(NULL);
			exit(EXIT_FAILURE);
		}
	}


	if(load_config(&config, config_path)) {
		exit(EXIT_FAILURE);
	}

	atexit(close_sequencer);
	atexit(destroy_fake_keyboard);
	atexit(free_config_file);

	signal(SIGINT, sighandler);
	signal(SIGTERM, sighandler);

	fprintf(stderr, "%u:%u\n", mididev, port);
	open_sequencer(seqname, fakename);
	create_fake_keyboard(uinput_path, fakename);
	receive_events(mididev, port);
}
